import './App.css';
import ReadMoreAndLess from 'react-read-more-less';
import ShowMoreText from 'react-show-more-text';

function App() {
    function executeOnClick(isExpanded) {
        console.log(isExpanded);
    }
  return (
    <div className="App">
    {/*  <ReadMoreAndLess*/}

    {/*    className="read-more-content"*/}
    {/*    charLimit={250}*/}
    {/*    readMoreText="Read more"*/}
    {/*    readLessText="Read less"*/}
    {/*>*/}
    {/*    sign is just a valid javascript identifier which is used as an alias for jQuery. Prototype, jQuery, and most javascript libraries use the $ as the primary base object (or function).*/}

    {/*    Most of them also have a way to relinquish the $ so that it can be used with another library that uses it. In that case you use jQuery instead of $. In fact, $ is just a shortcut for jQuery.*/}

    {/*    You can also read the following artcle to get a clear idea.*/}
    {/*</ReadMoreAndLess>*/}
        <ShowMoreText
            /* Default options */
            lines={3}
            more='Read more'
            less='Read less'
            className='content-css'
            anchorClass='my-anchor-css-class'
            onClick={executeOnClick}
            expanded={false}

        >
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
            ut labore et dolore magna amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
            minim veniam, quis nostrud exercitation ullamco laboris nisi
            ut aliquip ex Lorem ipsum dolor sit amet, consectetur
            adipiscing elit, sed do eiusmod tempor incididunt ut labore


        </ShowMoreText>
    </div>

  );
}

export default App;
